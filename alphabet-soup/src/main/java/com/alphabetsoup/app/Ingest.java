package com.alphabetsoup.app;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.lang.Character;
import java.lang.Integer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Ingest {

    public Ingest() { }

    /**
     * Ingests an input file and returns a Data object
     */
    public Data ingest(String fileName) {
        // Read file into stream, convert to list of Strings
        List<String> list = new ArrayList<>();
        
        try (Stream<String> stream = Files.lines(Paths.get(fileName))) {
            list = stream.collect(Collectors.toList());
            
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Create grid object
        int i = 0;
        while (i < list.get(0).length() 
              && Character.isDigit(list.get(0).charAt(i))) {
            i++;
        }
        int rowCt = Integer.parseInt(list.get(0).substring(0, i));
        i++;
        int colCt = Integer.parseInt(list.get(0).substring(i));
        
        // Remove 1st entry from list
        list.remove(0);

        // Move words list to new list
        List<String> words = new ArrayList<>();
        for (i = rowCt; i < list.size(); i++) {
            words.add(list.get(i));
        }
        while (list.size() > rowCt) {
            list.remove(rowCt);
        }

        Data data = new Data();
        data.setGrid(list);
        data.setWords(words);

        // Store mapping of 1st character to searchable word
        Map<Character, List<String>> lookup = new HashMap<>();
        for (String w : words) {
            Character c = w.charAt(0);
            if (!lookup.containsKey(c)) {
                lookup.put(c, new ArrayList<String>());
            }
            List<String> updated = lookup.get(c);
            updated.add(w);
            lookup.put(c, updated);
        }
        data.setLookup(lookup);
        return data;
    }
}