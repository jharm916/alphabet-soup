package com.alphabetsoup.app;

import java.util.List;

public class App 
{
    public static void main(String[] args)
    {
        // Get filename
        String fileName = args[0];

        // Ingest file containing:
        // Grid dimensions
        // Grid contents for Word Search
        // List of words contained in Word Search
        Data data = new Ingest().ingest(fileName);

        // Run search algorithm on data object
        List<String> outp = new Search().search(data);
        
        // Print output
        for (String s : outp) {
            System.out.println(s);
        }
    }
}