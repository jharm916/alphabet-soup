package com.alphabetsoup.app;

import java.util.List;
import java.util.Map;
import java.lang.Character;

/** 
 * Stores data for alphabet-soup application.
 */
public class Data {

    private List<String> grid;

    private List<String> words;

    private Map<Character, List<String>> lookup;

    public Data() { }

    /**
     * Public getter for grid
     */
    public List<String> getGrid() {
        return grid;
    }

    /**
     * Public setter for grid
     */
    public void setGrid(List<String> grid) {
        this.grid = grid;
    }

    /**
     * Public getter for words
     */
    public List<String> getWords() {
        return words;
    }
    
    /**
     * Public setter for words
     */
    public void setWords(List<String> words) {
        this.words = words;
    }

    /**
     * Public getter for lookup
     */
    public Map<Character, List<String>> getLookup() {
        return lookup;
    }

    /**
     * Public setter for lookup
     */
    public void setLookup(Map<Character, List<String>> lookup) {
        this.lookup = lookup;
    }

}