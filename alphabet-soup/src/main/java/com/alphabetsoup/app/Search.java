package com.alphabetsoup.app;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import java.lang.Character;

/**
 * Contains business logic for alphabet-soup application.
 */
public class Search {

    public Search() { }

    public List<String> search(Data data) {
        int rows = data.getGrid().size();
        int cols = data.getGrid().get(0).length();
        List<String> outp = new ArrayList<>();

        // Iterate over grid
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j+=2) {
                Character key = data.getGrid().get(i).charAt(j);

                if (data.getLookup().containsKey(key)) {
                    List<String> possibleWords = data.getLookup().get(key);

                    // Get adjacent coordinates to current grid position
                    List<int[]> adjacents = getAdjacents(i, j);

                    for (int[] adjacent : adjacents) {
                        // Check bounds of adjacent coordinates
                        if (!(adjacent[0] >= 0 && adjacent[0] < rows
                                && adjacent[1] >= 0 && adjacent[1] < cols)) {
                            continue;
                        }

                        char nextChar = data.getGrid()
                            .get(adjacent[0])
                            .charAt(adjacent[1]);

                        // Potential position in possibleWords entry
                        int position = 1;

                        // Try to match with possibleWords at position
                        List<String> removeWords = new ArrayList<>();
                        int[] source = null;
                        int[] currAdjacent = null;
                        int[] nextAdjacent = null;
                        int endi = -1, endj = -1;

                        // This solution will find the 1st occurrence of the word
                        // being searched for. It will not find multiple occurrences of the same word.
                        // This could be added by having a count value for words that are requested multiple
                        // times.
                        for (String s : possibleWords) {
                            if (s.charAt(position) == nextChar) {
                                boolean found = false;
                                if (position == s.length() - 1) {
                                    // 2 character word
                                    endi = adjacent[0];
                                    endj = adjacent[1];
                                    found = true;
                                }

                                // Keep checking characters in the same direction
                                while (position < s.length() - 1) {
                                    // The search structures model something like a list of coordinates in-order
                                    //  in the same direction. We want to be able to get the next coordinate
                                    // in a given direction.
                                    if (source == null) {
                                        source = new int[] { i, j };
                                        currAdjacent = new int[] { adjacent[0], adjacent[1] };
                                    } else {
                                        source = new int[] { currAdjacent[0], currAdjacent[1] };
                                        currAdjacent = new int[] { nextAdjacent[0], nextAdjacent[1] };
                                    }
                                    nextAdjacent = returnNextAdjacent(source, currAdjacent);
                                    position++;

                                    if (!(nextAdjacent[0] >= 0 && nextAdjacent[0] < rows
                                        && nextAdjacent[1] >= 0 && nextAdjacent[1] < cols)) {
                                        break;
                                    }
                                    nextChar = data.getGrid()
                                        .get(nextAdjacent[0])
                                        .charAt(nextAdjacent[1]);

                                    if (s.charAt(position) != nextChar) {
                                        break;
                                    }
                                    if (position == s.length() - 1) {
                                        endi = nextAdjacent[0];
                                        endj = nextAdjacent[1];
                                        found = true;
                                    }
                                }
                                if (found) {
                                    outp.add(generateOutput(s, i, j, endi, endj));
                                    removeWords.add(s);
                                }
                            }
                        }
                        // Remove found words from lookup map
                        Map<Character, List<String>> lookup = data.getLookup();
                        for (String s : removeWords) {
                            possibleWords.remove(s);
                        }
                        lookup.put(key, possibleWords);
                        data.setLookup(lookup);
                    }
                }
            }
        }

        // Return list of output strings
        return outp;
    }

    /** 
     * Returns the next adjacent in the same direction as adjacent
     * relative to source.
    */
    private int[] returnNextAdjacent(int[] source, int[] adjacent) {
        if (source[0] == adjacent[0] + 1 && source[1] == adjacent[1] + 2) {
            return new int[] { adjacent[0] - 1, adjacent[1] - 2 };

        } else if (source[0] == adjacent[0] + 1 && source[1] == adjacent[1]) {
            return new int[] { adjacent[0] - 1, adjacent[1] };

        } else if (source[0] == adjacent[0] + 1 && source[1] == adjacent[1] - 2) {
            return new int[] { adjacent[0] - 1, adjacent[1] + 2 };

        } else if (source[0] == adjacent[0] && source[1] == adjacent[1] + 2) {
            return new int [] { adjacent[0], adjacent[1] - 2 };

        } else if (source[0] == adjacent[0] && source[1] == adjacent[1] - 2) {
            return new int[] { adjacent[0], adjacent[1] + 2 };

        } else if (source[0] == adjacent[0] - 1 && source[1] == adjacent[1] + 2) {
            return new int[] { adjacent[0] + 1, adjacent[1] - 2 };
        
        } else if (source[0] == adjacent[0] - 1 && source[1] == adjacent[1]) {
            return new int[] { adjacent[0] + 1, adjacent[1] };

        } else if (source[0] == adjacent[0] - 1 && source[1] == adjacent[1] - 2) {
            return new int[] { adjacent[0] + 1, adjacent[1] + 2 };
        }

        // Should never return null, if it does, something is broken
        return null;
    }

    /**
     * Returns adjacent coordinates from (i, j)
     * @param i i coordinate in grid
     * @param j j coordinate in grid
     */
    private List<int[]> getAdjacents(int i, int j) {
        List<int[]> adjacents = new ArrayList<>();
        adjacents.add(new int[] { i - 1, j - 2 }); // up-left
        adjacents.add(new int[] { i - 1, j });     // up
        adjacents.add(new int[] { i - 1, j + 2 }); // up-right
        adjacents.add(new int[] { i, j - 2 });     // left
        adjacents.add(new int[] { i, j + 2 });     // right
        adjacents.add(new int[] { i + 1, j - 2 }); // down-left
        adjacents.add(new int[] { i + 1, j });     // down
        adjacents.add(new int[] { i + 1, j + 2 }); // down-right
        return adjacents;
    }
    
    /**
     * Generates output text in specified format
     * @param s word that was found
     * @param i starting i position
     * @param j starting j position
     * @param endi ending i position
     * @param endj ending j position
     */
    private String generateOutput(String s, int i, int j, int endi, int endj) {
        StringBuilder output = new StringBuilder();
        output.append(s + " ");
        output.append(i + ":" + j/2 + " ");
        output.append(endi + ":" + endj / 2);
        return output.toString();
    }
}