package com.alphabetsoup.app;

import static org.junit.Assert.assertEquals;
import org.junit.Test;
import java.util.List;

public class SearchTest 
{

    @Test
    public void testSearchInput1()
    {
        String fileName = "target/test-classes/input1"; 
        Data data = new Ingest().ingest(fileName);
        List<String> outp = new Search().search(data);
        assertEquals(outp.size(), 4);
        String[] expected = new String[] {
            "ABC 0:0 0:2",
            "AEI 0:0 2:2",
            "CEG 0:2 2:0",
            "FED 1:2 1:0"
        };
        for (int i = 0; i < expected.length; i++) {
            assertEquals(expected[i], outp.get(i));
        }
    }

    @Test
    public void testSearchInput2()
    {
        String fileName = "target/test-classes/input2";
        Data data = new Ingest().ingest(fileName);
        List<String> outp = new Search().search(data);
        assertEquals(outp.size(), 6);
        String[] expected = new String[] {
            "TYXH 0:0 0:3",
            "HFGE 0:3 3:0",
            "AG 1:0 1:1",
            "AGF 1:0 1:2",
            "CEFX 3:2 0:2",
            "SEGT 3:3 0:0"
        };
        for (int i = 0; i < expected.length; i++) {
            assertEquals(expected[i], outp.get(i));
        }
    }

    @Test
    public void testSearchInput3()
    {
        String fileName = "target/test-classes/input3";
        Data data = new Ingest().ingest(fileName);
        List<String> outp = new Search().search(data);
        assertEquals(outp.size(), 4);
        String[] expected = new String[] {
            "AQGQAEHAHA 0:9 9:9",
            "MONEY 2:4 6:4",
            "TOKEN 5:1 5:5",
            "ABSO 9:4 6:7"
        };
        for (int i = 0; i < expected.length; i++) {
            assertEquals(expected[i], outp.get(i));
        }
    }

    @Test
    public void testSearchInput4()
    {
        String fileName = "target/test-classes/input4";
        Data data = new Ingest().ingest(fileName);
        List<String> outp = new Search().search(data);
        assertEquals(outp.size(), 7);
        String[] expected = new String[] {
            "AQGQAEHAHA 0:9 9:9",
            "MONEY 2:4 6:4",
            "FOUNDER 2:17 8:11",
            "TOKEN 5:1 5:5",
            "ABSO 9:4 6:7",
            "TODAY 15:16 11:12",
            "INNATE 18:7 13:7"
        };
        for (int i = 0; i < expected.length; i++) {
            assertEquals(expected[i], outp.get(i));
        }
    }

    @Test
    public void testSearchInput5()
    {
        String fileName = "target/test-classes/input5";
        Data data = new Ingest().ingest(fileName);
        List<String> outp = new Search().search(data);
        assertEquals(outp.size(), 3);
        String[] expected = new String[] {
            "HELLO 0:0 4:4",
            "BYE 1:3 1:1",
            "GOOD 4:0 4:3"
        };
        for (int i = 0; i < expected.length; i++) {
            assertEquals(expected[i], outp.get(i));
        }
    }
}
